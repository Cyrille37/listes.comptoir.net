# listes.comptoir.net

Suivi du service Sympa sur https://listes.comptoir.net/

- les [tickets de suivi](https://framagit.org/Cyrille37/listes.comptoir.net/-/issues)
- le [wiki](https://framagit.org/Cyrille37/listes.comptoir.net/-/wikis/home)
- les graphs sur la distribution des emails
  - [mailgraph](https://listes.comptoir.net/mailgraph/)
  - [deliveryStatus](https://listes.comptoir.net/deliveryStatus/deliveryStatus.html)

Le serveur
- [Hetzner cx31](https://www.hetzner.com/cloud) (2c 8G 40Go)
- pages de statut:
  - [hetrixtools](https://hetrixtools.com/report/uptime/166b9229110665ca632e166c11171ebe/)

## Doc

- Sympa
  - [wikis/sympa](https://framagit.org/Cyrille37/listes.comptoir.net/-/wikis/sympa)
  - Dkim est géré par Sympa `/etc/sympa/sympa/sympa.conf`
- SpamAssassin
  - [wikis/spamassassin](https://framagit.org/Cyrille37/listes.comptoir.net/-/wikis/spamassassin)
  - configuration `/etc/default/spamassassin`
  - mise à jour dans `/etc/cron.daily/spamassassin`
- public listening ports
  - 80 & 443 nginx
  - 25 & 465 postfix
  - xx ssh
- Crowdsec
  - [wikis/crowdsec](https://framagit.org/Cyrille37/listes.comptoir.net/-/wikis/crowdsec)
  - abonné à la Blocklist « OTX Georgs Honeypot List »
  - collections: crowdsecurity/base-http-scenarios 0.6, crowdsecurity/http-cve 2.0, crowdsecurity/linux 0.2, crowdsecurity/nginx 0.2, crowdsecurity/sshd 0.2
  - local scenario: cyrille37-http-no-wordpress-here

## Changes

### 2024-03-02

DKIM
- Sympa
  - C'est lui qui s'occupe de la signature
  - selector: default._domainkey.listes.comptoir.net
- Postfix
  - Utilise opendkim
  - Selector: mail._domainkey.comptoir.net.
  - `smtpd_milters = local:spamass/spamass.sock, local:opendkim/opendkim.sock`

```
tools:~$ sudo ls -l /etc/dkimkeys/
drwxr-xr-x 2 opendkim opendkim 4096 Mar  2 20:16 comptoir.net
drwxr-xr-x 2 opendkim opendkim 4096 Oct 15  2021 listes.comptoir.net
```

État des lieux
- System: Ubuntu Jammy (22.04.4 LTS) (GNU/Linux 5.15.0-97-generic x86_64)
- Postfix 3.6.4
- Sympa 6.2.66
- SpamAssassin 3.4.6
- crowdsec v1.6.0

```
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        38G   15G   22G  40% /
```

### 2023-05-07

- postfix pour utiliser spamassassin comme "milter" au lieu d'élément de "transport" avec [spamass-milter](https://manpages.ubuntu.com/manpages/bionic/man1/spamass-milter.1.html) #18
  - les spams ne sont plus "marqués" mais transférés à root@comptoir.net
- Upgrade from Ubuntu Focal (20.04 lts) to Jammy (22.04 lts) #20
- Sympa 6.2.66
- SpamAssassin 3.4.6
- Crowdsec 1.4.6
